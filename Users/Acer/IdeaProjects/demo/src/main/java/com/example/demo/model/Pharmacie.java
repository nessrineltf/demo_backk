package com.example.demo.model;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Pharmacie implements Serializable {
//    public enum Type {
//        Jour,
//        Nuit,
//        Garde
//    }
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String nomPharmacie;
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date ouvertA;
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fermerA;
    private String typepharmacie;
    private String adresse;
    private int telephone;
    @ManyToOne
    private Ville ville;

    public Pharmacie() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomPharmacie() {
        return nomPharmacie;
    }

    public void setNomPharmacie(String nomPharmacie) {
        this.nomPharmacie = nomPharmacie;
    }

    public Date getOuvertA() {
        return ouvertA;
    }

    public void setOuvertA(Date ouvertA) {
        this.ouvertA = ouvertA;
    }

    public Date getFermerA() {
        return fermerA;
    }

    public void setFermerA(Date fermerA) {
        this.fermerA = fermerA;
    }

    public String getTypepharmacie() {
        return typepharmacie;
    }

    public void setTypepharmacie(String typepharmacie) {
        this.typepharmacie = typepharmacie;
    }

    public Ville getVille() {
        return ville;
    }

    public void setVille(Ville ville) {
        this.ville = ville;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getTelephone() {
        return telephone;
    }

    public void setTelephone(int telephone) {
        this.telephone = telephone;
    }
}
