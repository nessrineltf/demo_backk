package com.example.demo.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Hopitale implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String nomHopitale;
    private int tel_urgence;
    private int tel_secondaire;
    private String nomDirecteur;
    private String adresse;
    @ManyToOne
    private Ville ville;

    public Hopitale() {
    }

    public Long getId() {
        return id;
    }

    public String getNomHopitale() {
        return nomHopitale;
    }

    public void setNomHopitale(String nomHopitale) {
        this.nomHopitale = nomHopitale;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getTel_urgence() {
        return tel_urgence;
    }

    public void setTel_urgence(int tel_urgence) {
        this.tel_urgence = tel_urgence;
    }

    public int getTel_secondaire() {
        return tel_secondaire;
    }

    public void setTel_secondaire(int tel_secondaire) {
        this.tel_secondaire = tel_secondaire;
    }

    public String getNomDirecteur() {
        return nomDirecteur;
    }

    public void setNomDirecteur(String nomDirecteur) {
        this.nomDirecteur = nomDirecteur;
    }

    public Ville getVille() {
        return ville;
    }

    public void setVille(Ville ville) {
        this.ville = ville;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
}
