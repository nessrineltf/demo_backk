package com.example.demo.model;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;


@Entity
public class DossierMedical implements Serializable {

    public enum Sexe{ Homme, Femme}

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(unique = true)
    private Long numDossier;
    @Column(unique = true)
    private Long N_cnss;
    private String groupe_sangine;
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date date_Admission;
    private Sexe sexe;
    private String relation;


    public DossierMedical() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumDossier() {
        return numDossier;
    }

    public void setNumDossier(Long numDossier) {
        this.numDossier = numDossier;
    }

    public Long getN_cnss() {
        return N_cnss;
    }

    public void setN_cnss(Long n_cnss) {
        N_cnss = n_cnss;
    }

    public String getGroupe_sangine() {
        return groupe_sangine;
    }

    public void setGroupe_sangine(String groupe_sangine) {
        this.groupe_sangine = groupe_sangine;
    }

    public Date getDate_Admission() {
        return date_Admission;
    }

    public void setDate_Admission(Date date_Admission) {
        this.date_Admission = date_Admission;
    }

    public Sexe getSexe() {
        return sexe;
    }

    public void setSexe(Sexe sexe) {
        this.sexe = sexe;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }
}
