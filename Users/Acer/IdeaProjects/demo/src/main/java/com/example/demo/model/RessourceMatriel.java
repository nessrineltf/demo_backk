package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
@Entity
public class RessourceMatriel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int quantitéEtree;
    private int quantitéSortie;

    public RessourceMatriel(int quantitéEtree, int quantitéSortie) {
        this.quantitéEtree = quantitéEtree;
        this.quantitéSortie = quantitéSortie;
    }

    public RessourceMatriel() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getQuantitéEtree() {
        return quantitéEtree;
    }

    public void setQuantitéEtree(int quantitéEtree) {
        this.quantitéEtree = quantitéEtree;
    }

    public int getQuantitéSortie() {
        return quantitéSortie;
    }

    public void setQuantitéSortie(int quantitéSortie) {
        this.quantitéSortie = quantitéSortie;
    }
}
