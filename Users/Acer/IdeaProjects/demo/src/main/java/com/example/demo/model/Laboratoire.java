package com.example.demo.model;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
@Entity
public class Laboratoire implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String nomLabo;
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date ouvertA;
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fermerA;
    private String adresse;
    private int telephone;
    @ManyToOne
    private Ville ville;
    public Laboratoire() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomLabo() {
        return nomLabo;
    }

    public void setNomLabo(String nomLabo) {
        this.nomLabo = nomLabo;
    }

    public Date getOuvertA() {
        return ouvertA;
    }

    public void setOuvertA(Date ouvertA) {
        this.ouvertA = ouvertA;
    }

    public Date getFermerA() {
        return fermerA;
    }

    public void setFermerA(Date fermerA) {
        this.fermerA = fermerA;
    }

    public Ville getVille() {
        return ville;
    }

    public void setVille(Ville ville) {
        this.ville = ville;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getTelephone() {
        return telephone;
    }

    public void setTelephone(int telephone) {
        this.telephone = telephone;
    }
}
